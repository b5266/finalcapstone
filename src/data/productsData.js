const productsData = [
	{
		id: "item001",
		name: "Chipping Gun",
		description: "Genuine Item 11/16inches (17mm Hex Shank), Demolition hammer (900w), Sliding chuck provides tool-less chisel change by simply sliding down the chuck collar and installing the shank",
		price: 7999,
		onOffer: true
	},
	{
		id: "item002",
		name: "Electric Drill",
		description: "Impact Drill, Hammer Drill with Variable speed 680w Super Select ID68016P.",
		price: 999,
		onOffer: true
	},
	{
		id: "item003",
		name: "Cut Off Machine",
		description: " Some quick example text to build on the card title and make up the bulk of the card's content.",
		price: 3099,
		onOffer: true
	},
	{
		id: "item004",
		name: "Cicular Saw",
		description: " Some quick example text to build on the card title and make up the bulk of the card's content.",
		price: 2750,
		onOffer: true
	},
	{
		id: "item005",
		name: "Welding Machine",
		description: " Some quick example text to build on the card title and make up the bulk of the card's content.",
		price: 4500,
		onOffer: true
	},
	{
		id: "item006",
		name: "Power Washer",s
		description: " Some quick example text to build on the card title and make up the bulk of the card's content.",
		price: 2975,
		onOffer: true
	},
];
export default productsData;
