import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card style={{ width: '19rem' }} className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>Electric Drill</h2>
				    </Card.Title>
				    <Card.Text>
				      Genuine Item 11/16inches (17mm Hex Shank), Demolition hammer (900w), Sliding chuck provides tool-less chisel change by simply sliding down the chuck collar and installing the shank",
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card style={{ width: '19rem' }} className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>Electric Drill</h2>
				    </Card.Title>
				    <Card.Text>
				      		Genuine Item 11/16inches (17mm Hex Shank), Demolition hammer (900w), Sliding chuck provides tool-less chisel change by simply sliding down the chuck collar and installing the shank
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card style={{ width: '19rem' }} className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>Cut Off Machine</h2>
				    </Card.Title>
				    <Card.Text>
				      Genuine Item 11/16inches (17mm Hex Shank), Demolition hammer (900w), Sliding chuck provides tool-less chisel change by simply sliding down the chuck collar and installing the shank",
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}