import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import ProductCard from '../components/ProductCard';

export default function Home(){

	const data = {
	    title: "San Andres Hardware",
	    content: "Construction Materials on your Door step. ",
	    destination: "/products",
	    label: "Purchase now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}
